# asyncdispatch

This package provides a module called `asyncdispatch`, which is a decorator that allows you to easily dispatch to either a sync or an async implementation of a function based on how its being invoked. When it's called normally, it will run the sync definition of the function. When it's called using `await`, it will run the async definition of the function.

