"""A decorator to dispatch between sync/async versions of a function."""

import sys
from functools import wraps
from types import ModuleType


# Ideally we could use setuptools_scm, but that would require using setuptools
# which cannot yet use metadata in `pyproject.toml`. Flit is used so that we
# can take full advantage of `pyproject.toml` as the one spot for all project
# metadata, but the trade-off is we need to specify the version number in code,
# rather than through Git. Hopefully in the future setuptools will be able to
# fetch metadata from `pyproject.toml` instead of `setup.cfg`.
__version__ = '0.0.1'


class AsyncDispatchModule(ModuleType):
    """asyncdispatch is a callable module - it exposes the docstring above."""

    __version__ = __version__

    def __call__(self, f):
        """TODO: this docstring."""
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)
        return wrapper


sys.modules[__name__] = AsyncDispatchModule(__name__, __doc__)
