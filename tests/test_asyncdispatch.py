"""Tests for asyncdispatch."""

import asyncdispatch


def test_call_module():
    """Test that the module object is a decorator."""
    @asyncdispatch
    def f(x, y, z=2):
        return (z, y, x)

    assert f(0, 1) == (2, 1, 0)
